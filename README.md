# Passwd_Parsing

**Passwd_Parsing_Code_Challenge**

Requirement for this challenge: Pythonn 3 and above version

How to run this code:
*  This project include 2 files, 1.Parse.py 2.path.json
*  Keep both the files in same folder
*  Before running you can edit the paths of Passwd and Group file in path.json
*  Run Parse.py file
*  It will fetch input from path.json
*  As a output you will see json object in console/terminal
*  You can also run this python file in cron job